import {Component, OnInit} from '@angular/core';
import {UploadService} from '../upload.service';
import {HttpEventType} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";
import {DatePipe} from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  selectedFiles = [];
  progress: number = 0;

  constructor(private uploadService: UploadService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  onFileSelected(event: any) {
    this.selectedFiles = event.target.files;
  }

  onUpload() {
    const formData = new FormData();
    for (let file of this.selectedFiles) {
      formData.append("files", file);
    }

    this.uploadService.upload(formData).subscribe((event: any) => {
      if (event.type == HttpEventType.UploadProgress) {
        this.snackBar.open('Status', 'File upload in progress.');
        this.progress = Math.round((event.loaded / event.total) * 50);
      }
      if (event.type == HttpEventType.DownloadProgress) {
        this.snackBar.open('Status', 'File resize in progress.');
        this.progress = this.progress + Math.round((event.loaded / event.total) * 50);
      }
      if (event.type == HttpEventType.Response) {
        if (event.status == 200) {
          this.snackBar.open('Status', 'File resize completed successfully.', {duration: 3500})

          const url = window.URL.createObjectURL(event.body);
          let anchor = document.createElement("a");
          const datePipe: DatePipe = new DatePipe('en-US')
          let formattedDate = datePipe.transform(new Date(), 'YYYY-MM-DD-HH-mm-ss')
          anchor.download = 'image-resized ' + formattedDate + '.zip';
          anchor.href = url;
          anchor.click();
          this.selectedFiles = [];
        } else {
          this.snackBar.open('Status', "Error while resizing file, " + event.status);
        }
      }
    })
  }

}

