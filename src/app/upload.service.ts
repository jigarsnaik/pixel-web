import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpErrorResponse, HttpEventType, HttpResponse} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  SERVER_URL: string = "http://18.138.228.237:8080/pixel-app/image/resize";

  constructor(private httpClient: HttpClient) {
  }

  public upload(formData: FormData) {
    console.log("Inside Service")
    console.log(formData.getAll('files').length);
    // @ts-ignore
    return this.httpClient.post<any>(this.SERVER_URL, formData, { responseType: 'blob', reportProgress: true, observe: "events"});
  }

}


