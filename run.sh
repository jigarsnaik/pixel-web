# build image
docker build -t pixel-web .

# create container and run
docker run -p 4200:4200 -t -it --name pixel-web pixel-web

